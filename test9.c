#include <stdio.h>
void swap(float *a,float *b);
int main()
{
    float x,y;
    printf("enter 2 numbers:\n");
    printf("num1=");
    scanf("%f",&x);
    printf("num2=");
    scanf("%f",&y);
    swap(&x,&y);
    printf("so after swapping the numbers:\n num1=%.2f \n num2=%.2f",x,y);
    return 0;
}
void swap(float *a,float *b)
{
    float temp;
    temp=*a;
    *a=*b;
    *b=temp;

}
