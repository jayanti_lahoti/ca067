#include <stdio.h>
int main()
{
    int mat[3][3],trans[3][3],i,j;
    printf("enter the elements of matrix:\n");
    for(i=0;i<3;i++)
    {
        printf("enter the %d row\n",i+1);
         for(j=0;j<3;j++)
    {
         scanf(" %d",&mat[i][j]);
    }

    }
     printf("\nthe matrix is:\n");
    for(i=0;i<3;i++)
    {
        printf("\n");
        for(j=0;j<3;j++)
        {
            printf("\t %d",mat[i][j]);
        }
    }
    printf("\n\n");
    printf("the transpose is:");
    for(i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
        {
            trans[i][j]=mat[j][i];
        }
    }
    for(i=0;i<3;i++)
    {
        printf("\n");
        for(j=0;j<3;j++)
        {
            printf("\t%d",trans[i][j]);
        }
    }
    return 0;
}
